package com.Sudoku.rest;

import com.Sudoku.service.SudokuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import java.util.Set;

@RestController
public class UserApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiController.class);

    private SudokuService sudokuService;

    @PostMapping("/api/sudoku/verify")
    public ResponseEntity<Map<String, Set<Integer>>> login(@RequestParam(name = "fileName") String fileName) {

        sudokuService = new SudokuService(fileName + ".csv");
        Map<String, Set<Integer>> mistakes = sudokuService.getMistakes();

        if(mistakes.isEmpty()){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return ResponseEntity.badRequest().body(mistakes);
        }
    }
}
