package com.Sudoku.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class SudokuService implements SudokuServiceInterface {

    private final String fileName;
    private final int SIZE = 9;
    private  final int AREA_SIZE = 3;
    private final int[][] gameTable;
    private final Set<Integer> lineIds = new TreeSet<>();
    private final Set<Integer> columnIds = new TreeSet<>();
    private final Set<Integer> areaIds = new TreeSet<>();
    private final Map<String, Set<Integer>> mistakes = new HashMap<>();

    public SudokuService(String fileName){
        this.fileName = fileName;
        this.gameTable = getGameTable();
        checkSudoku();
    }

    @Override
    public List<String> readCsv() {
        List<String> file = new ArrayList<>();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(this.fileName));
            String line;
            while ((line = reader.readLine()) != null) {
                file.add(line);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return file;
    }

    @Override
    public int[][] getGameTable(){
        List<String> csv = readCsv();
        int[][] tab = new int[SIZE][SIZE];
        for(int i = 0; i < csv.size(); i++){
            String[] tmp = csv.get(i).split(";");
            for(int j = 0; j < tmp.length; j++){
                tab[i][j] = Integer.parseInt(tmp[j]);
            }
        }
        return tab;
    }

    @Override
    public void checkSudoku() {

        // ROWS

        int rowCounter = 0;
        for(int[] tab : gameTable){
            Set<Integer> set = new HashSet<>();
            rowCounter++;
            for(int val : tab){
                if(!set.contains(val)){
                    set.add(val);
                }else{
                    lineIds.add(rowCounter);
                }
            }
        }

        // COLUMNS

        for(int i = 0; i < SIZE; i++){
            Set<Integer> set = new HashSet<>();
            for(int j = 0; j < SIZE; j++){
                if(!set.contains(gameTable[j][i])){
                    set.add(gameTable[j][i]);
                }else {
                    columnIds.add(i + 1);
                }
            }
        }

        // AREAS

        int areaCounter = 1;
        for(int i = 0; i < SIZE/AREA_SIZE; i++) {
            for (int j = 0; j < SIZE/AREA_SIZE; j++) {
                Set<Integer> set = new HashSet<>();
                for(int k = 0; k < SIZE/AREA_SIZE; k++) {
                    for (int m = 0; m < SIZE/AREA_SIZE; m++) {
                        int row = ( AREA_SIZE * i ) + k;
                        int col = ( AREA_SIZE * j ) + m;
                        int val = gameTable[row][col];
                        if(!set.contains(val)){
                            set.add(val);
                        } else {
                            areaIds.add(areaCounter);
                        }
                    }
                }
                areaCounter++;
            }
        }
    }

    @Override
    public Map<String, Set<Integer>> getMistakes() {
        if(!lineIds.isEmpty()){
            mistakes.put("lineIds", lineIds);
        }
        if(!columnIds.isEmpty()){
            mistakes.put("columnIds", columnIds);
        }
        if(!areaIds.isEmpty()){
            mistakes.put("areaIds", areaIds);
        }
        return mistakes;
    }

}
