package com.Sudoku.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SudokuServiceInterface {

    List<String> readCsv();

    int[][] getGameTable();

    void checkSudoku();

    Map<String, Set<Integer>> getMistakes();
}
